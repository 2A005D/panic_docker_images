#!/bin/bash

#########################################
# 	First Install Script (Basics)
#########################################
# Maintainer:	dont panic it-services
# Author: 		Marco Huber mh@panic.at
# Date: 			03.07.2018
#########################################


DATE=$(date "+%d %b %H:%M")

# Settings
SSHD=/panic/logs/install_sshd.log

echo "$DATE" > $SSHD

#Basic Settings
	# Locales
		sed -i "s/#\ en_US.UTF-8\ UTF-8/en_US.UTF-8\ UTF-8/g" /etc/locale.gen
		locale-gen

	# Set Timezone
		ln -sf /usr/share/zoneinfo/Europe/Vienna /etc/localtime

	# OpenSSH Server
		cp -r /panic/setup/debian/.ssh/ /root/
		sed -i "s/#PermitRootLogin\ prohibit-password/PermitRootLogin\ yes/g" /etc/ssh/sshd_config
		sed -i "s/#PasswordAuthentication\ yes/PasswordAuthentication\ no/g" /etc/ssh/sshd_config
		echo "PermitRootLogin = yes" >> $SSHD
		systemctl enable sshd
		systemctl restart sshd

	# Bash
		cp -r /panic/setup/debian/.bashrc /root/

	# VIM
		echo "set mouse-=a" > /root/.vimrc

	# Motd - Banner
		cat /panic/dontpanic > /etc/motd

	# Write Logfile
		DATE=$(date "+%d %b %H:%M")
		echo "Basic Setup Finished $DATE" >> $SSHD

# Remove Configs
	rm -r -f /panic/setup/debian

# Safe Basic Installation
	rm -f /panic/ctrl/firstrun
	touch /panic/ctrl/run
