#!/bin/bash

#########################################
# 	Firstrun Script
#########################################
# Maintainer:	dont panic it-services
# Author: 	Marco Huber mh@panic.at
# Date: 	10.06.2018
#########################################

LOGPATH=/panic/logs
DATE=$(date "+%d %b %H:%M")

# First Run
	if [ -e /panic/ctrl/firstrun ] && [ ! -e /panic/ctrl/run ]; then
		/panic/install_basic.sh >> $LOGPATH/basic_install.log
	fi


# Logging
	if [ -e /panic/ctrl/run  ] && [ ! -e /panic/ctrl/firstrun ];then
		echo "run at $DATE" >> $LOGPATH/panic_run.log
	fi
