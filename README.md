Overview
========
Maintainer: mh@panic.at
Version:    2.0

Build Images
============
Panic Debain Jessie Systemd
-------------------
```sh
cd panic_debian-jessie
docker build -t panic_debian-jessie-systemd:2.0 .
```

Panic Debain Stretch Systemd
-------------------
```sh
cd panic_debian-stretch
docker build -t panic_debian-stretch-systemd:2.0 .
```

Run Container
=============
```sh
repo_app-name	<port><container_number>:<port>	image_name
panic_app	2201:22 -d 				panic_debian-stretch
```

Debian Jessie Systemd
----------------------
```sh
docker run -d --privileged --name c01_<name> -v /sys/fs/cgroup:/sys/fs/cgroup:ro -p 2201:22 panic_debian-jessie-systemd:2.0
```
Installation will be completed in about 1 minutes

Debian Stretch Systemd
----------------------
```sh
docker run -d --privileged --name c01_<name> -v /sys/fs/cgroup:/sys/fs/cgroup:ro -p 2201:22 panic_debian-stretch-systemd:2.0
```
Installation will be completed in about 1 minutes


Example
=======
Container 2
-----------
```sh
docker run -d --privileged --name c02_<name> -v /sys/fs/cgroup:/sys/fs/cgroup:ro -p 2202:22 panic_debian-stretch-systemd:2.0
```

Container 24
------------
```sh
docker run -d --privileged --name c24_<name> -v /sys/fs/cgroup:/sys/fs/cgroup:ro -p 2224:22 panic_debian-stretch-systemd:2.0
```

Example Specific Network and map Ports 80 443 and 6556 to Container
===============================================================
```sh
docker run -d --privileged --name c02_<name> --net main-net --ip 172.18.0.2 -v /sys/fs/cgroup:/sys/fs/cgroup:ro -p 2201:22 -p 8001:80 -p 4431:443 -p 6556 panic_debian-stretch-systemd:2.0
```

Example Network Link to other Container and map Ports 80 443 and 6556 to Container
===============================================================
```sh
docker run -d --privileged --name c01_<name> --link c02_<name> -v /sys/fs/cgroup:/sys/fs/cgroup:ro -p 2201:22 -p 8001:80 -p 4431:443 -p 6556 panic_debian-stretch-systemd:2.0
```
